# Microservices Workshop
Lab 02: Meeting the Application

---

# Tasks

 - Build the application
 
 - Meet the application
 
 - Meet the application API
  
 - Inspect the sources

---
&nbsp;

## Build the application (in the infrastructure server)

 - Clone the application "all-in-one" repository using the command below:
 
```
$ git clone https://github.com/selaworkshops/microservices-calculator-app-all.git ~/app
$ cd app
```

 - Update the HOST_IP environment variable in the ui-service:
 
```
$ vim docker-compose.yml
```
```
environment:
     - HOST_IP=<infrastructure-server-ip>
```

 - To build the application use docker-compose:
 
```
$ docker-compose up -d
```

 - Ensure the containers are running by run:
 
```
$ docker-compose ps
```

(note that the cloned repository uses "git subtrees" to join several repositories under a single one)


&nbsp;


## Meet the application:

 - Browse to the application main page (ui-service) using the url below:
 
```
http://<infrastructure-server-ip>:3000
```

 - Set 2 numbers and click "calculate"

<img alt="Image 2.1" src="images/task-2.1.png"  width="75%" height="75%">

&nbsp;

## Meet the application API:

&nbsp;

 - Access the application UI using the url below:
 
```
http://<infrastructure-server-ip>:3000
```

 - Access the Sum API using the url below:
 
```
http://<infrastructure-server-ip>:3001/sum/12/13
```

 - Access the Division API using the url below:
 
```
http://<infrastructure-server-ip>:3004/division/15/3
```

 - Access the Multiplication API using the url below:
 
```
http://<infrastructure-server-ip>:3003/multiplication/8/3
```

 - Access the Subtraction API using the url below:
 
```
http://<infrastructure-server-ip>:3002/subtraction/8/2
```
 
 
&nbsp;

## Cleanup

 - Remove the application from the infrastructure server:
 
```
$ docker-compose down
```

&nbsp;

## Inspect the sources:

 - sum-service:  
  
https://github.com/selaworkshops/sum-service


 - subtraction-service:  

https://github.com/selaworkshops/subtraction-service


 - multiplication-service:  

https://github.com/selaworkshops/multiplication-service


 - division-service:  

https://github.com/selaworkshops/division-service


 - ui-service:  

https://github.com/selaworkshops/ui-service


&nbsp;

 